class HomePageController < ApplicationController
  
  def home
    @question = getQuestion
    @word = Word.new
  end
  
  def new 
  end
  
  def create
    render 'home'
  end
  
  private
  
    def getQuestion
      if (1 + rand(2) == 1)
        return "what is the synonym of " + Word.random + " ?"
      end
      return "what is the antonym of " + Word.random + "?"
    end
end
