class WordsController < ApplicationController
  @@choice = 0   #what to ask for a word? synonym or antonym
  @@randomWord = nil #random word to be asked
  @@score = 0     #score of the player
  def new
    @@choice = 1 + rand(2)  
#    @action4 = "Quit"    #
    @@randomWord = getRandomWord 
    @question = getQuestion(@@randomWord)   #stores question to be asked from player
    @word = Word.new    
    @@score = 0
    @action1 = "Guess the Word"    #display text button of button
  end
  
  def create
    @action4 = nil
    @word = Word.new
    if (!params[:commit].match("Quit") && @@score >= 0)
      @message = nil    #it stores ionformation like congrats or sorry depending on whether player entered correct or wrong word
  #    @word = Word.new
   #   @word.name = params[:word][:name]
      question = Word.new
      question.name = @@randomWord
      if params[:commit].match("Guess the Word")      # condition when user guesses the word
        if ((@@choice == 1 && question.synonyms.include?(params[:word][:name].downcase)) || (@@choice == 2 && question.antonyms.include?(params[:word][:name].downcase)))       
                #player entered correct answer block          
          @@score += 10
          @@randomWord = getRandomWord      
          @@choice = 1 + rand(2)
          @question = getQuestion(@@randomWord)
          @action1 = "Guess the Word"
          @message = "Congrats your answer is right!!!"
        else  #player entered wrong answer block
          @question = getQuestion(@@randomWord)
          @@score -= 2
          @action1 ="Try Again"
          @action2 = "Show Hint"
          @action3 = "Another Word"
          @message = "Sorry your answer is wrong!!!"
        end
      elsif params[:commit].match("Try Again") #player tried to answer again
        if ((@@choice == 1 && question.synonyms.include?(params[:word][:name].downcase)) || (@@choice == 2 && question.antonyms.include?(params[:word][:name].downcase)))
          #player answered correct after trying agagin
          @@score += 10
          @@randomWord = getRandomWord      
          @@choice = 1 + rand(2)
          @question = getQuestion(@@randomWord)
          @action1 = "Guess the Word"
          @message = "Congrats your answer is right!!!"
        else
          player answered wrong after trying agagin
          @@score -= 2
          @question = getQuestion(@@randomWord)
          @action1 ="Try Again"
          @action2 = "Show Hint"
          @action3 = "Another Word"
          @message = "Sorry your answer is wrong!!!"
        end      
      elsif params[:commit].match("Show Hint")  #player asked for hint
        @hint = getHint(@@randomWord)
        @question = getQuestion(@@randomWord)
        @@score -= 1
        @action1 ="Try Again"
        @action2 = "Show Hint"
        @action3 = "Another Word"
      else  #playerasker for another word
        @answer = "The possible answers are " + @@randomWord
        @action1 = "Guess the Word"
        @@score -= 5     #i added this myself. if he chooses to change word, he loses points
        @@randomWord = getRandomWord      
        @@choice = 1 + rand(2)
        @question = getQuestion(@@randomWord)
        
      end
      @myScore = "Your score is " + @@score.to_s + " ."
      @action4 = "Quit"
    else #player quits the game or heis points go below zero
      @quit = "Quit"
    end
    render 'new'
  end
  
  def show
  end

  private
    def getQuestion(word)
#      @randomWord = Word.random
      if (@@choice == 1)
        return "what is the synonym of " + word + " ?"
      end
      return "what is the antonym of " + word + "?"
    end    
    
    def getHint(word)
      temp = Word.new
      temp.name = word
      choice = 1 + rand(4)
      if choice == 1
        return "The jumbled word is " + temp.jumble
      elsif choice == 2
        temp = Word.new
        temp.name = word
        synonyms = temp.synonyms
        if (synonyms.length == 0)
          return "Sorry there are no synonyms of this word"
        end
        return "The synonym of word is " + synonyms
      else
        temp = Word.new
        temp.name = word
        antonyms = temp.antonyms
        if (antonyms.length == 0)
          return "Sorry there are no antonyms of this word"        
        end
        return "The antonym of word is " + antonyms
      end
    end
    
    def getAnswer(question)
      if @@choice == 1
        return "The synonyms of " + @@randomWord.name + "are" + question.synonyms
      else
        return "The antonyms of " + @@randomWord.name + "are" + question.antonyms
      end
    end
    
    def getRandomWord   #Make sure that the word has antonyms and synonyms
      randomWord = Word.new
      randomWord.name = Word.random
      while (randomWord.synonyms.empty? || randomWord.antonyms.empty?) do
        randomWord.name = Word.random
      end
      return randomWord.name
    end
end
