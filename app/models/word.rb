class Word #< ActiveRecord::Base
  include ActiveModel::Conversion
  extend ActiveModel::Naming
#  include ActiveModel::Validations  
  attr_accessor :name
  
  def persisted?
    false
  end
  
#  def setName(name)
 #   self.name = name
  #end
  
  def definitions
    Wordnik.word.get_definitions(self.name)
  end

  def synonym
    ans = Wordnik.word.get_related(self.name, :type => 'synonym', :use_canonical => true)
    if !ans.empty?
      return ans.first["words"][0]
    end
    return nil.to_a
#    Wordnik.word.get_related(self.name, :type => 'synonym', :use_canonical => true, :limit => 1).first["words"][0]
  end

  def synonyms
    ans = Wordnik.word.get_related(self.name, :type => 'synonym', :use_canonical => true)
    if !ans.empty?
      return ans.first["words"]
    end
    return nil.to_a
#    Wordnik.word.get_related(self.name, :type => 'synonym', :use_canonical => true).first["words"]
  end  

  def antonym
    ans = Wordnik.word.get_related(self.name, :type => 'antonym', :use_canonical => true)
    if !ans.empty?
      return ans.first["words"][0]
    end
    return nil.to_a
  end
  
  def antonyms
    ans = Wordnik.word.get_related(self.name, :type => 'antonym', :use_canonical => true)
    if !ans.empty?
      return ans.first["words"][0]
    end
    return nil.to_a
  end  
  # Get one or many random words
  # Word.random(:limit => 10)
  # Word.random
  def self.random(options={})

    # Defaults
    options.reverse_merge!(
      :min_corpus_count => 1000,
      :min_dictionary_count => 100,
      :limit => 1
    )
    
    # Get back an array of hashes that look like {"id"=>92367, "word"=>"resist"} 
    # and turn them into Word objects:
#    words = (options).map{|obj.name| Word.new(obj['word']) }
    Wordnik.words.get_random_word(options)['word']
  end
  
  def jumble
    return self.name.split('').shuffle.join
  end
  
end